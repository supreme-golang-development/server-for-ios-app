package main

import (
	"time"

	"github.com/jinzhu/gorm"
)

type UsersDB struct {
	gorm.Model
	UUID        string
	Timestamp   time.Time
	DeepLink    string
	UserFBID    string
	UserIP      string
	ValidStatus bool
	URL         string
	Query       string
	Status      string
	CountryCode string
	Region      string
	RegionName  string
	City        string
	Timezone    string
	Isp         string
	Org         string
	AsData      string
	Reverse     string
	Mobile      bool
}

type UserData struct {
	UUID     string `json:"uuid"`
	DeepLink string `json:"deeplink"`
	UserFBID string `json:"fbid"`
	UserIP   string `json:"-"`
	IsValid  bool   `json:"-"`
}

type AppResponse struct {
	IsValid bool   `json:"isvalid"`
	URL     string `json:"url"`
}

type Geo struct {
	gorm.Model
	Query       string `json:"query"`
	Status      string `json:"status"`
	CountryCode string `json:"countryCode"`
	Region      string `json:"region"`
	RegionName  string `json:"regionName"`
	City        string `json:"city"`
	Timezone    string `json:"timezone"`
	Isp         string `json:"isp"`
	Org         string `json:"org"`
	As          string `json:"as"`
	Reverse     string `json:"reverse"`
	Mobile      bool   `json:"mobile"`
}
type Links struct {
	Link []struct {
		Name   string `json:"name"`
		URL    string `json:"url"`
		Active bool   `json:"active"`
	} `json:"link"`
}
