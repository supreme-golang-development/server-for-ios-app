package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/gramework/gramework"
	_ "github.com/lib/pq"
	"github.com/valyala/fasthttp"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "alligator_iot"
	dbname   = "users"
)

var (
	errInternal = errors.New("Internal Server Error")
)

var allowedCountries = []string{
	"BY",
	"RU",
}

func addUserGORM(data UsersDB) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Println("Couldn't connect to db: ", err)
		panic(err)
	}
	defer db.Close()
	db.AutoMigrate(&UsersDB{}) // Migrate models
	emptyUser := UsersDB{}
	db.Where("uuid = ?", data.UUID).First(&emptyUser) // Find existing UUID
	if emptyUser.UUID != data.UUID {
		db.Create(&data)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func getActiveLink() (string, error) {
	links := &Links{}
	jsonFile, err := os.Open("static/config.json")
	check(err)
	dec := json.NewDecoder(jsonFile)
	for {
		if err := dec.Decode(&links); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		fmt.Println(links.Link[0].Name)
	}

	return links.Link[0].URL, nil
}

func validateIP(ctx *gramework.Context, ip string) (bool, *Geo, error) {
	url := "http://ip-api.com/json/" + ip + "?fields=130846"
	status, body, err := fasthttp.Get(nil, url)

	log := ctx.Logger.WithField("ip", ip)

	if err != nil {
		log.WithError(err).Error("could not get IP info")
		return false, nil, errInternal
	}

	if status < 200 || status >= 400 {
		log.WithField("status", status).Error("got unexpected status from geo service")
		return false, nil, errInternal
	}

	geoInfo := &Geo{}
	_, err = ctx.UnJSONBytes(body, geoInfo)
	if err != nil {
		log.WithError(err).Error("Could not parse geo info")
		return false, geoInfo, errInternal
	}

	for _, country := range allowedCountries {
		if geoInfo.CountryCode == country {
			return true, geoInfo, nil
		}
	}

	return false, geoInfo, nil
}

func main() {

	app := gramework.New()
	app.POST("/api/post", func(ctx *gramework.Context) (interface{}, error) {
		data := &UserData{}
		appResponse := &AppResponse{}
		body := ctx.PostBody()
		_, err := ctx.UnJSONBytes(body, data)
		if err != nil {
			return fmt.Println("JSON is empty or corrupted")
		}
		data.UserIP = ctx.RemoteIP().String()
		isValid, geoInfo, err := validateIP(ctx, data.UserIP)
		if err != nil {
			return fmt.Println("Error validating IP")
		}
		data.IsValid = isValid
		if isValid {
			appResponse.IsValid = isValid
			appResponse.URL, _ = getActiveLink()
		} else {
			appResponse.IsValid = isValid
			appResponse.URL = "empty"
		}
		user := &UsersDB{
			UUID:        data.UUID,
			Timestamp:   time.Now(),
			DeepLink:    data.DeepLink,
			UserFBID:    data.UserFBID,
			UserIP:      data.UserIP,
			ValidStatus: isValid,
			URL:         appResponse.URL,
			Query:       geoInfo.Query,
			Status:      geoInfo.Status,
			CountryCode: geoInfo.CountryCode,
			Region:      geoInfo.Region,
			RegionName:  geoInfo.RegionName,
			City:        geoInfo.City,
			Timezone:    geoInfo.Timezone,
			Isp:         geoInfo.Isp,
			Org:         geoInfo.Org,
			AsData:      geoInfo.As,
			Reverse:     geoInfo.Reverse,
			Mobile:      geoInfo.Mobile,
		}
		addUserGORM(*user)
		return appResponse, nil
	})
	app.ServeFile("/api/json", "static/config.json")
	app.SPAIndex("static/index.html")
	app.ListenAndServe()
}
